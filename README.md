## visualizing Weighted Social Network model

This application visualizes a time evolution of social network formation using a weighted social network model.
The simulation is based on the model proposed by Kumpula et al. (http://journals.aps.org/prl/abstract/10.1103/PhysRevLett.99.228701)

You'll see how community structures are developed.

## How to run

- First of all, you need [processing](http://processing.org/).
- The code depends on [toxiclibs](http://toxiclibs.org/).
  - Download toxiclibs from the website, and copy to the library directory for processing.
    - For Mac, put the library to `~/Documents/Processing/libraries`.
- Open `weighted_social_network_model.pde` in processing, and run it.

## License

All of the source code is licensed under the GNU Lesser General Public License.
http://www.gnu.org/licenses/lgpl-2.1.html

This code is developing based on Shiffman's code. I thank him for making such interesting examples.
His original code is on [github](https://github.com/shiffman/The-Nature-of-Code-Examples).

